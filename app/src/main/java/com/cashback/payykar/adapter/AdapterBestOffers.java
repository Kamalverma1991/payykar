package com.cashback.payykar.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cashback.payykar.R;

import java.util.ArrayList;


/**
 * Project           : DoseraNew
 * File Name         : AdapterBestOffers
 * Description       : TODO: Enter description
 * Revision History: version 1:
 * Date: 2/12/17
 * Original author: pradeep
 * Description: Initial version
 */
public class AdapterBestOffers extends RecyclerView.Adapter<HolderBestOffers> {

    private ArrayList<String> listofHashOffers;
    private Context context;


    public AdapterBestOffers(Context context, ArrayList<String> listofHashOffers) {
        this.context = context;
        this.listofHashOffers = listofHashOffers;
    }


    @Override
    public HolderBestOffers onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.customview_bestoffers, null);
        HolderBestOffers rcv = new HolderBestOffers(layoutView, listofHashOffers);

        return rcv;
    }

    @Override
    public void onBindViewHolder(HolderBestOffers holder, int position) {
//        holder.tv_appname.setText(listofHashOffers.get(position).getSurveyName());
//        holder.tv_description.setText("Get Rs. " + listofHashOffers.get(position).getPayout());
//        holder.bt_install.setText(listofHashOffers.get(position).getButtonDescription());

    }

    @Override
    public int getItemCount() {
        return this.listofHashOffers.size();
    }


}
