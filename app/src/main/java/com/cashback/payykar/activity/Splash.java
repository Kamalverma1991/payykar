package com.cashback.payykar.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.cashback.payykar.R;

/**
 * Created by dell on 3/2/2017.
 */

public class Splash extends AppCompatActivity {

    private TextView tvPayykar;
    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        handler = new Handler();
        tvPayykar = (TextView) findViewById(R.id.textpaykar);
        tvPayykar.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/customsursive.ttf"));
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            }
        }, 1500);
    }
}
