package com.cashback.payykar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class HolderBestOffers extends RecyclerView.ViewHolder implements View.OnClickListener {

    //    public TextView tv_appname, tv_description, bt_install;
//    public ImageView icon;
    private ArrayList<String> listofHashOffers;

    public HolderBestOffers(View itemView, ArrayList<String> listofHashOffers) {
        super(itemView);
        this.listofHashOffers = listofHashOffers;
        itemView.setOnClickListener(this);

//        tv_appname = (TextView) itemView.findViewById(R.id.textview_appname);
//        tv_description = (TextView) itemView.findViewById(R.id.textview_description);
//        bt_install = (TextView) itemView.findViewById(R.id.button_install);
//        icon = (ImageView) itemView.findViewById(R.id.imageview_icon);
    }


    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Offer Position = " + getLayoutPosition(), Toast.LENGTH_SHORT).show();

    }


//    interface OnOfferRedirectingUser{
//        public void onRedirect(int position);
//    }
}