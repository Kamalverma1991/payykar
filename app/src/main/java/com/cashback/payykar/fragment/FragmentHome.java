package com.cashback.payykar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.cashback.payykar.R;
import com.cashback.payykar.adapter.AdapterBestOffers;

import java.util.ArrayList;


/**
 * Created by Kamal Verma on 12/19/2015.
 */
public class FragmentHome extends Fragment implements View.OnClickListener {


    private RecyclerView recyclerView;
    private AdapterBestOffers adapter;
    private ArrayList<String> list;
    private ViewFlipper viewFlipper;
    private LinearLayout llPrepaid;
    private LinearLayout llPostpaid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Home");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = new ArrayList<>();
        list.add("play");
        list.add("wewe");
        list.add("rere");
        list.add("fgfgf");
        list.add("hhhh");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        list.add("ujuj");
        initView(view);
        viewFlipperWorks();
    }

    private void initView(View view) {
        adapter = new AdapterBestOffers(getActivity(), list);
        viewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);
        llPrepaid = (LinearLayout) view.findViewById(R.id.linear_recharge_prepaid);
        llPostpaid = (LinearLayout) view.findViewById(R.id.linear_recharge_postpaid);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView myList = (RecyclerView) view.findViewById(R.id.recyclerview_offers);
        myList.setLayoutManager(layoutManager);
        myList.setAdapter(adapter);
        llPrepaid.setOnClickListener(this);
        llPostpaid.setOnClickListener(this);
    }

    private void viewFlipperWorks() {
        for (int i = 0; i < 3; i++) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            if (i == 0) {
                imageView.setImageResource(R.drawable.bg1);
            }
            if (i == 1) {
                imageView.setImageResource(R.drawable.bg2);
            }
            if (i == 2) {
                imageView.setImageResource(R.drawable.bg3);
            }
            viewFlipper.addView(imageView);
        }
        viewFlipper.startFlipping();
//        viewFlipper.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent bannerclick = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(viewFlipper.indexOfChild(viewFlipper.getCurrentView())).getRedirectURL()));
//                startActivity(bannerclick);
//            }
//        });


    }

    @Override
    public void onClick(View view) {
        if (view == llPrepaid) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.container, new FragmentAnimation(), "Animation");
            ft.commit();

        } else if (view == llPostpaid) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.container, new FragmentAnimation(), "Animation");
            ft.commit();
        }
    }
}
